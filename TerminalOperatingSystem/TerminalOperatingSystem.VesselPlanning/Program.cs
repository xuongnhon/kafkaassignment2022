﻿using Confluent.Kafka;
using Confluent.Kafka.SyncOverAsync;
using Confluent.SchemaRegistry;
using Confluent.SchemaRegistry.Serdes;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using TerminalOperatingSystem.MessagingApi.VesselBooking;

var configurationRoot = new ConfigurationBuilder()
    .SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
    .AddJsonFile("appsettings.json", true)
    .AddEnvironmentVariables()
    .Build();

var bootstrapServers = configurationRoot.GetValue<string>("BootstrapServers");
var topicName = configurationRoot.GetValue<string>("TopicName");
var consumerGroupId = configurationRoot.GetValue<string>("ConsumerGroupId");
var schemaRegistryUrl = configurationRoot.GetValue<string>("SchemaRegistryUrl");

var consumerConfig = new ConsumerConfig
{
    BootstrapServers = bootstrapServers,
    GroupId = consumerGroupId
};

var schemaRegistryConfig = new SchemaRegistryConfig
{
    Url = schemaRegistryUrl
};

var cancellationTokenSource = new CancellationTokenSource();
var consumeTask = Task.Run(() =>
{
    using (var schemaRegistry = new CachedSchemaRegistryClient(schemaRegistryConfig))
    using (
        var consumer = new ConsumerBuilder<string, VesselVisitAdded>(consumerConfig)
            .SetValueDeserializer(new AvroDeserializer<VesselVisitAdded>(schemaRegistry).AsSyncOverAsync())
            .SetErrorHandler((_, e) => Console.WriteLine($"Error: {e.Reason}"))
            .Build())
    {
        consumer.Subscribe(topicName);

        try
        {
            while (true)
            {
                try
                {
                    var consumeResult = consumer.Consume(cancellationTokenSource.Token);
                    var vesselVisitAdded = consumeResult.Message.Value;
                    Console.WriteLine($"A new vessel visit has been added (Id = {vesselVisitAdded.VesselVisitId})");
                    Console.WriteLine("Start planning for the new vessel visit");
                    Console.WriteLine("Done");
                }
                catch (ConsumeException e)
                {
                    Console.WriteLine($"Consume error: {e.Error.Reason}");
                }
            }
        }
        catch (OperationCanceledException)
        {
            consumer.Close();
        }
    }
});

Console.WriteLine("VesselPlanning application started");
consumeTask.Wait();
Console.WriteLine("VesselPlanning application stopped");
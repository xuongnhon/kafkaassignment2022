﻿using TerminalOperatingSystem.MessagingApi.VesselBooking;

namespace TerminalOperatingSystem.VesselBooking
{
    public static class ServiceCollectionExtension
    {
        public static void RegisterDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IMessageProducer<VesselVisitAdded>, VesselVisitAddedProducer>();
        }
    }
}

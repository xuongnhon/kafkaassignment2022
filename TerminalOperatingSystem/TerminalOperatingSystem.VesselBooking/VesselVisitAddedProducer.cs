﻿using TerminalOperatingSystem.MessagingApi.VesselBooking;

namespace TerminalOperatingSystem.VesselBooking
{
    public class VesselVisitAddedProducer : BaseProducer<VesselVisitAdded>
    {
        public VesselVisitAddedProducer(IConfiguration configuration, ILogger<BaseProducer<VesselVisitAdded>> logger) : base(configuration, logger)
        {

        }
    }
}

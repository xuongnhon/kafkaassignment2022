using Grpc.Core;
using TerminalOperatingSystem.MessagingApi.Grpc.Service;
using TerminalOperatingSystem.MessagingApi.VesselBooking;

namespace TerminalOperatingSystem.VesselBooking
{
    public class GrpcService : MessagingApi.Grpc.Service.VesselBooking.VesselBookingBase
    {
        private readonly IMessageProducer<VesselVisitAdded> _vesselVisitAddedProducer;
        private readonly ILogger<GrpcService> _logger;

        public GrpcService(IMessageProducer<VesselVisitAdded> vesselVisitAddedProducer, ILogger<GrpcService> logger)
        {
            _vesselVisitAddedProducer = vesselVisitAddedProducer;
            _logger = logger;
        }

        public override async Task<VesselVisitAddResponse> AddVesselVisit(VesselVisitAddRequest request, ServerCallContext context)
        {
            _logger.LogInformation($"Processing a new {nameof(VesselVisitAddRequest)}");

            var newVesselVisitId = Guid.NewGuid();
            _logger.LogInformation($"Added a new vessel visit (Id = {newVesselVisitId})");

            var vesselVisitAdded = new VesselVisitAdded
            {
                VesselVisitId = newVesselVisitId
            };

            _logger.LogInformation($"Sending {nameof(VesselVisitAdded)}");
            await _vesselVisitAddedProducer.SendAsync(vesselVisitAdded);
            _logger.LogInformation($"{nameof(VesselVisitAdded)} was sent");

            var response = new VesselVisitAddResponse
            {
                Id = newVesselVisitId.ToString()
            };

            return response;
        }
    }
}
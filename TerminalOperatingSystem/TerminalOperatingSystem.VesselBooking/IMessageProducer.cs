﻿namespace TerminalOperatingSystem.VesselBooking
{
    public interface IMessageProducer<T>
    {
        public Task SendAsync(T message);
    }
}

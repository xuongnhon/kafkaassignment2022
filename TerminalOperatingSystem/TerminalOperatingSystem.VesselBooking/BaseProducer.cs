﻿using Confluent.Kafka;
using Confluent.SchemaRegistry;
using Confluent.SchemaRegistry.Serdes;

namespace TerminalOperatingSystem.VesselBooking
{
    public abstract class BaseProducer<T> : IMessageProducer<T>
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<BaseProducer<T>> _logger;

        public BaseProducer(IConfiguration configuration, ILogger<BaseProducer<T>> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task SendAsync(T message)
        {
            var bootstrapServers = _configuration["BootstrapServers"];
            var topicName = _configuration["TopicName"];
            var schemaRegistryUrl = _configuration["SchemaRegistryUrl"];
            var producerConfig = new ProducerConfig
            {
                BootstrapServers = bootstrapServers
            };

            var schemaRegistryConfig = new SchemaRegistryConfig
            {
                Url = schemaRegistryUrl
            };

            var avroSerializerConfig = new AvroSerializerConfig
            {
                BufferBytes = 100
            };

            _logger.LogInformation($"Start sending {typeof(T).Name}");

            using (var schemaRegistry = new CachedSchemaRegistryClient(schemaRegistryConfig))
            using (
                var producerBuilder = new ProducerBuilder<string, T>(producerConfig)
                    .SetValueSerializer(new AvroSerializer<T>(schemaRegistry, avroSerializerConfig))
                    .Build()
            )
            {
                await producerBuilder
                    .ProduceAsync(topicName, new Message<string, T> { Key = Guid.NewGuid().ToString(), Value = message })
                    .ContinueWith(task =>
                    {
                        if (!task.IsFaulted)
                        {
                            _logger.LogInformation($"Produced to: {task.Result.TopicPartitionOffset}");
                            return;
                        }

                        _logger.LogError($"Error producing message: {task.Exception?.InnerException}");
                    });
            }

            _logger.LogInformation($"End sending {typeof(T).Name}");
        }
    }
}
